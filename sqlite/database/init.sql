CREATE TABLE IF NOT EXISTS users
(
    id       INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS temperature_sensor_data
(
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    temperature REAL     NOT NULL,
    unit        TEXT     NOT NULL CHECK (unit IN ('C', 'F')),
    timestamp   DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);


INSERT INTO users (username, password)
VALUES ('user1', 'password1');
INSERT INTO users (username, password)
VALUES ('user2', 'password2');

INSERT INTO temperature_sensor_data (temperature, unit)
VALUES (25.3, 'C');
INSERT INTO temperature_sensor_data (temperature, unit)
VALUES (22.1, 'C');
INSERT INTO temperature_sensor_data (temperature, unit)
VALUES (1.3, 'C');
INSERT INTO temperature_sensor_data (temperature, unit)
VALUES (19.1, 'C');
INSERT INTO temperature_sensor_data (temperature, unit)
VALUES (18.3, 'C');
INSERT INTO temperature_sensor_data (temperature, unit)
VALUES (6.1, 'C');
INSERT INTO temperature_sensor_data (temperature, unit)
VALUES (45.3, 'C');
INSERT INTO temperature_sensor_data (temperature, unit)
VALUES (58.1, 'C');
INSERT INTO temperature_sensor_data (temperature, unit)
VALUES (12.3, 'C');
INSERT INTO temperature_sensor_data (temperature, unit)
VALUES (17.1, 'C');

