import re


def check(data, pattern):
    pattern = re.compile(pattern)
    match = pattern.match(data)
    return match
