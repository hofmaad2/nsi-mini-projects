import sqlite3

from flask import Blueprint, request, session, jsonify

DATABASE = './sqlite/database/db.sqlite'

api = Blueprint('api', __name__)


def get_db_connection():
    conn = sqlite3.connect(DATABASE)
    conn.row_factory = sqlite3.Row
    return conn


@api.route("/insertValue", methods=["POST"])
def insert_value():
    value_to_insert = request.data.decode('utf-8').strip()

    if (value_to_insert is None) or (not value_to_insert.replace('.', '', 1).isdigit()):
        return jsonify({"error": "Please insert a valid value"}), 400

    temp = float(value_to_insert)

    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute('INSERT INTO temperature_sensor_data (temperature, unit) VALUES (?, ?)',
                (temp, 'C'))
    conn.commit()
    conn.close()

    new_entry = {"temp": temp}
    return jsonify({"message": "Value inserted successfully", "new_entry": new_entry}), 201


@api.route("/mostCurrentValue", methods=["GET"])
def get_most_current_value():
    if session.get("username") is None:
        return jsonify({"error": "Unauthorized"}), 401

    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        'SELECT temperature AS temp FROM temperature_sensor_data ORDER BY timestamp DESC LIMIT 1')
    result = cur.fetchone()
    conn.close()

    if result:
        return jsonify(dict(result))
    else:
        return jsonify({"error": "No data found"}), 404


@api.route("/lastValues/<int:num_values>", methods=["GET"])
def get_last_values(num_values):
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        'SELECT temperature, timestamp AS temp FROM temperature_sensor_data ORDER BY timestamp DESC LIMIT ?',
        (num_values,))
    last_values = cur.fetchall()
    conn.close()

    return jsonify([dict(row) for row in last_values])


@api.route('/deleteOldest', methods=['DELETE'])
def delete_last_elements():
    num = request.args.get('n', default=1, type=int)

    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute(
        '''DELETE FROM temperature_sensor_data 
        WHERE id IN (
            SELECT id FROM temperature_sensor_data 
            ORDER BY timestamp ASC
            LIMIT ?)''',
        (num,))
    conn.commit()
    conn.close()

    return jsonify({"message": f"Successfully removed the oldest {num} elements."}), 200
