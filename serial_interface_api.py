import serial
import database
from datetime import datetime
import data_format_checker


def listen_to_serial():
    print("Listening to serial...")
    ser = serial.Serial('/dev/ttyUSB0', 115200, timeout=3)

    while True:
        line = ser.readline()
        if line:
            data = line.decode().strip()
            match = data_format_checker.check(data, r'^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}),(\d+\.\d+)$')
            if match:
                timestamp_str, temp_str = match.groups()
                temp = float(temp_str)
                timestamp = datetime.strptime(timestamp_str, '%Y-%m-%dT%H:%M:%S')

                print("Valid data received:", timestamp, temp)

                unit = "C"

                conn = database.get_db_connection()
                cur = conn.cursor()
                cur.execute('INSERT INTO temperature_sensor_data (timestamp, temperature, unit) VALUES (?, ?, ?)',
                            (timestamp, temp, unit))
                conn.commit()
                conn.close()
            else:
                print("Received invalid data:", data)
