import paho.mqtt.client as mqtt
from datetime import datetime
import database

import data_format_checker

broker = "broker.hivemq.com"
api_topic = "esp32/temperature"
username = "mqtt_api"
password = "mqtt_appi_password"
port = 1883
keepalive = 60


def listen_to_mqtt():
    client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION1)
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(broker, port, keepalive)
    client.loop_forever()


def on_connect(client, userdata, flags, reason_code):
    print("Connected with result code " + str(reason_code))
    client.subscribe(api_topic)
    print("Subscribed to topic " + api_topic)


def on_message(client, userdata, msg):
    data = msg.payload.decode()
    print("Received message: " + data)

    match = data_format_checker.check(data, r'^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}),(\d+\.\d+)$')
    if match:
        timestamp_str, temp_str = match.groups()
        temp = float(temp_str)
        timestamp = datetime.strptime(timestamp_str, '%Y-%m-%dT%H:%M:%S')
        print("Valid data received:", timestamp, temp)

        unit = "C"

        conn = database.get_db_connection()
        cur = conn.cursor()
        cur.execute('INSERT INTO temperature_sensor_data (timestamp, temperature, unit) VALUES (?, ?, ?)',
                    (timestamp, temp, unit))
        conn.commit()
        conn.close()
    else:
        print("Received invalid data:", data)
