from flask import Flask, render_template, redirect, request, session, flash
from api_routes import api
import sqlite3
from werkzeug.security import generate_password_hash, check_password_hash
import threading
import database
import serial_interface_api
import mqtt_api

app = Flask(__name__)
app.register_blueprint(api, url_prefix='/api')
app.secret_key = 'key boi'


@app.route("/")
def base():
    return redirect("login")


@app.route("/login", methods=["POST", "GET"])
def login():
    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")

        conn = database.get_db_connection()
        cur = conn.cursor()
        cur.execute('SELECT * FROM users WHERE username = ?', (username,))
        user = cur.fetchone()
        conn.close()

        if user and check_password_hash(user['password'], password):
            session['username'] = user['username']
            return redirect("/dashboard")
        else:
            flash("Invalid username or password.")
            return redirect("/login")

    elif request.method == "GET":
        return render_template('login.html')


@app.route("/register", methods=["POST", "GET"])
def register():
    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")

        if not username or not password:
            flash("Username and password are required.")
            return redirect("/register")

        hashed_password = generate_password_hash(password)

        conn = database.get_db_connection()
        try:
            cur = conn.cursor()
            cur.execute('INSERT INTO users (username, password) VALUES (?, ?)', (username, hashed_password))
            conn.commit()
        except sqlite3.IntegrityError:
            flash("Username already exists.")
            return redirect("/register")
        finally:
            conn.close()
        return redirect("/login")

    elif request.method == "GET":
        return render_template('register.html')


@app.route("/logout")
def logout():
    session.pop("username", None)
    return redirect("/login")


@app.route("/dashboard", methods=["POST", "GET"])
def dashboard():
    if session.get("username") is None:
        return render_template("login.html")

    if request.method == "GET":
        conn = database.get_db_connection()
        cur = conn.cursor()
        cur.execute('SELECT timestamp, temperature AS temperature FROM temperature_sensor_data')
        temperature_data = cur.fetchall()
        conn.close()

        return render_template('dashboard.html', display_user_options=True, username=session['username'],
                               pageName="Dashboard",
                               static_values=temperature_data)

    if request.method == "POST":
        action = request.form.get("action")
        if action == "remove":
            num_elements_str = request.form.get("num_elements", "0")
            try:
                num_elements_to_remove = int(num_elements_str)
            except ValueError:
                num_elements_to_remove = 0

            if num_elements_to_remove > 0:
                conn = database.get_db_connection()
                cur = conn.cursor()
                cur.execute('''
                        DELETE FROM temperature_sensor_data
                        WHERE id IN (
                            SELECT id FROM temperature_sensor_data
                            ORDER BY timestamp ASC
                            LIMIT ?
                        )
                    ''', (num_elements_to_remove,))
                conn.commit()
                conn.close()

            return redirect("/dashboard")

        elif action == "update":
            conn = database.get_db_connection()
            cur = conn.cursor()
            num_elements_str = request.form.get("num_elements")
            try:
                num_elements = int(num_elements_str)
            except ValueError:
                num_elements = 0

            cur.execute('SELECT timestamp, temperature FROM temperature_sensor_data ORDER BY timestamp DESC LIMIT ?',
                        (num_elements,))
            temperature_data = cur.fetchall()
            conn.close()
            return render_template('dashboard.html', display_user_options=True, username=session['username'],
                                   pageName="Dashboard",
                                   static_values=temperature_data)

        return redirect("/dashboard")


if __name__ == '__main__':
    serial_thread = threading.Thread(target=serial_interface_api.listen_to_serial)
    serial_thread.start()
    mqtt_thread = threading.Thread(target=mqtt_api.listen_to_mqtt)
    mqtt_thread.start()
    app.run(host="0.0.0.0", port=5000, debug=True, use_reloader=False)
