#include <Arduino.h>
#include <ctime>

unsigned long currentTime;
const unsigned long interval = 20000;
unsigned long lastMeasuredTime = 0;

void printFormattedTime()
{
    struct tm timeinfo {};
    if (!getLocalTime(&timeinfo)) {
        Serial.println("Failed to obtain time");
        return;
    }

    char buffer[30];
    strftime(buffer, sizeof(buffer), "%Y-%m-%dT%H:%M:%S", &timeinfo);
    double temperature = random(200, 300) / 10.0;

    String data = String(buffer) + "," + String(temperature);

    Serial.println(data);
}

void setup()
{
    Serial.begin(115200);
    while (!Serial) { ;
    }

    struct tm tm{};
    tm.tm_year = 121;
    tm.tm_mon = 0;
    tm.tm_mday = 1;
    tm.tm_hour = 12;
    tm.tm_min = 0;
    tm.tm_sec = 0;
    time_t t = mktime(&tm);

    struct timeval now = {.tv_sec = t};
    settimeofday(&now, nullptr);
}

void loop()
{
    currentTime = millis();
    if (currentTime - lastMeasuredTime >= interval) {
        printFormattedTime();
        lastMeasuredTime = currentTime;
    }
}
